import csv
import pkgutil
import importlib

def dumpStatsToCsv(statsPath, stats):
    with open(statsPath, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=['Resets Learning','Inputs Learning', 'Resets Testing', 'Inputs Testing'])
        writer.writeheader()
        for (learnerResets, learnerInputs, testResets, testInputs) in stats:
            writer.writerow({'Resets Learning': learnerResets, 'Inputs Learning': learnerInputs, 'Resets Testing':testResets,'Inputs Testing': testInputs})

def plotTraceUsingPlotly(py, trace, name):
    if len(trace) > 0:
        plot_url = py.iplot(trace, filename=name, privacy="public")
        print name,'plot can be found at', plot_url.resource
        
def plotUsingPlotly(learnerStats):
    if pkgutil.find_loader("plotly") is not None:
        import plotly.plotly as py
        import plotly.graph_objs as go
        lResetTraces = []
        lInputTraces = []
        for (learner, stats) in learnerStats.items():
            learnResets = map(lambda x: x[0], stats)
            learnInputs = map(lambda x: x[1], stats)
            resetTrace = go.Scatter(y=learnResets, name=learner)
            inputTrace = go.Scatter(y=learnInputs, name=learner)
            lResetTraces.append(resetTrace)
            lInputTraces.append(inputTrace)
        plotTraceUsingPlotly(py, lResetTraces, "Learner Resets")
        plotTraceUsingPlotly(py, lInputTraces, "Learner Inputs")
    else:
        print "The plotly package could not be found, so no graphs\
        can be drawn"
    