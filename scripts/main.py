import sys
import gatherer
import rabuilder
import plotter
import runner
import os
from paths import *

maxLength = 10
minLength = 2
step = 1
csvFile = None
model = os.path.join(modelsPath,"test.xml")
cmd = None

def printUsage():
    print "Usage:\n python main.py stats (learner[,learner]*)|all minSize maxSize step [csvFile]\n"
    print " python main.py build size [modelPath] "
    quit()
    
def getAllLearners():
    return [item.replace("getNumbers","") for item in dir(gatherer) if "getNumbers" in item]

def getFuncForLearner(module, learner, method):
    return getattr(module, method + learner)  
    
if len(sys.argv) == 1:
    printUsage()
else:
    cmd = sys.argv[1]
    if cmd == "stats":
        selectedLearners = sys.argv[2]
        learners = []
        if selectedLearners == 'all':
            learners = ['Tomte','Ralib']
        else:
            learners = selectedLearners.split(',')
        minLength = int(sys.argv[3])
        maxLength = int(sys.argv[4])
        step = int(sys.argv[5])
        if len(sys.argv) > 6:
            csvFile = sys.argv[6]
    elif cmd == "build":
        size = int(sys.argv[2])
        if len(sys.argv) > 3:
            modelPath = sys.argv[3]
        else:
            modelPath = os.path.join(modelsPath, "test.xml")
    else:
        printUsage()
if cmd == "build":
    print "Generating model of size",size
    rabuilder.generateRa(model, size)
else:
    def getFunctionsForLearners(learners):
        runFunctions = map(lambda x: getFuncForLearner(runner, x, "run"), learners)
        gathererFunctions = map(lambda x: getFuncForLearner(gatherer, x, "getNumbers"), learners)
        return dict(zip(learners, zip(runFunctions, gathererFunctions)))

    def collectStats(learners, minLength, maxLength, step):
        runnersAndGatherers = getFunctionsForLearners(learners)
        # initialize dict with [] entries for each learner, ie: {"tomte":[],"ralib":[]} 
        #statsForLearners = dict(zip(learners,[[]]*len(learners))) works, but it fills uses the same empty list to fill all elements
        statsForLearners = dict()
        for learner in learners:
            statsForLearners[learner]=[]
        #print statsForLearners
        
        for i in xrange(minLength,maxLength, step):
            rabuilder.generateRa(model, i)
            for (learner,(runner, gatherer)) in runnersAndGatherers.items():
                print learner, "learning", model, "of size", str(i)
                learnerOutput = runner(model)
                learnerStats = gatherer(learnerOutput)
                statsForLearners[learner].append(learnerStats)
        return statsForLearners
    
    def dumpStatsToCsv(learnerStats):
        for (learner, stats) in learnerStats.items():
            plotter.dumpStatsToCsv(os.path.join(resultsPath, learner + "_" + csvFile), stats)
            
    def dumpStatsToSdout(learnerStats):
        for (learner, stats) in learnerStats.items():
            print learner, ":", stats
    
    learnerStats = collectStats(learners, minLength, maxLength, step)
        
    if csvFile is not None:
        dumpStatsToCsv(learnerStats)
    else:
        dumpStatsToSdout(learnerStats)
        
    plotter.plotUsingPlotly(learnerStats)