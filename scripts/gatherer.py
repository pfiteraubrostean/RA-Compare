import re

def getStatsRa(statRegex, output):
    statLine = re.search(statRegex, output).group(0)
    statsNum = re.search('[0-9]+', statLine).group(0)
    return statsNum

def getStatsTomte(statRegex, output):
    statLine = re.findall(statRegex, output)[-1]
    statsNum = re.search('[0-9]+', statLine).group(0)
    return statsNum
    
def getNumbersTomte(output):
    learnResets = getStatsTomte(".*Learn\sresets.*",output)
    learnInputs = getStatsTomte(".*Learn\sinputs.*",output)
    testResets = getStatsTomte(".*Testing\sresets.*",output)
    testInputs = getStatsTomte(".*Testing\sinputs.*",output)
    return (learnResets, learnInputs, testResets, testInputs)
    
    
def getNumbersRalib(output):
    learnResets = getStatsRa(".*Resets\sLearning:.*",output)
    learnInputs = getStatsRa(".*Inputs\sLearning:.*",output)
    testResets = getStatsRa(".*Resets\sTesting:.*",output)
    testInputs = getStatsRa(".*Inputs\sTesting:.*",output)
    print getStatsRa(".*Locations:.*",output)
    return (learnResets, learnInputs, testResets, testInputs)