import os

scriptPath = os.path.realpath(__file__) 
libPath = os.path.join(os.path.dirname(scriptPath), "..", "lib")
inputPath = os.path.join(os.path.dirname(scriptPath), "..", "input")
modelsPath = os.path.join(os.path.dirname(scriptPath), "..", "models")
resultsPath = os.path.join(os.path.dirname(scriptPath), "..", "results")