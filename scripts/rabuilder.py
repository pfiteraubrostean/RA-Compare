import os
scriptPath = os.path.realpath(__file__) 
corePath = os.path.join(os.path.dirname(scriptPath), "core.xml")

def state(i):
    return "s" + str(i)

def rmloc(i):
    return "m" + str(i+1) + "_" + str(i) 
    
def mloc(i):
    return "m" + str(i) +"_" + str(i+1)

def location(label, initial=False):
    msg = "<location name=\"" + label +"\""
    if initial:
        msg += " initial=\"true\""
    msg += "/>"
    return msg
    
    
def locations(n, additionalLoc = None):
    msg = "<locations>\n"
    if additionalLoc is not None:
        for loc in additionalLoc:
            msg += location(loc) + "\n"
    for i in range(0,n+1):
        msg += location(state(i), initial= i == 0) + "\n"
    for i in range(0,n):    
        msg += location(mloc(i)) + "\n"
        msg += location(rmloc(i)) + "\n"
    msg += "</locations>"
    return msg
    
    
def trans(fr, to, action, params = None, assignments = None, guards = None):
    msg = "<transition from=\"" + fr+"\" to=\"" + to+"\" symbol=\""+ action +"\"" 
    if params is not None:
        msg += " params=\"" + params + "\""
    if assignments is None and guards is None:
        msg = msg + "/>"
    else:
        msg = msg + ">"
        if assignments is not None:
            msg = msg + "\n<assignments>"
            for (reg, param) in assignments:
                msg = msg + "\n" + "<assign to=\"" + reg + "\">" + param + "</assign>"
            msg = msg + "\n</assignments>"
        if guards is not None:
            for (reg, rel, param) in guards:
                msg = msg + "\n" + "<guard>" + reg + rel + param + "</guard>"
        msg = msg + "</transition>"
    return msg

def generateRa(filename, n):
    tempfile = open(filename, "w+")
    temp = open(corePath).read()

    temp +="\n" + locations(n, ["me_ok","me_nok"])
    temp +="\n" + "<transitions>"
    temp +="\n" + trans(state(0), mloc(0), "IGet", params="p", assignments=[("r0","p")])  
    temp +="\n" + trans(mloc(0), state(1), "OOK")  
    for i in range(1,n):
        temp +="\n" + trans(state(i),mloc(i), "IGet", params="p") 
        temp +="\n" + trans(mloc(i),state(i+1), "OOK")
    temp +="\n" + trans(state(n), "me_ok", "IGet", params="p", guards=[("p","==","r0")])  
    temp +="\n" + trans("me_ok", state(n), "OOK")
    temp +="\n" + trans(state(n), "me_nok", "IGet", params="p", guards=[("p","!=","r0")])  
    temp +="\n" + trans("me_nok", state(n), "ONOK")
    temp +="\n" + "</transitions>"
    temp +="\n" + "</register-automaton>"
    tempfile.write(temp)

    
