import subprocess
import shutil
import io
from paths import *

def runTomte(modelFile):
    command = ['tomte_learn.bat', '-e' ,"--config-option", "sutImplementation_modelFile=" +os.path.relpath(modelFile, os.path.join(inputPath, "tomte-0.4")),
    os.path.join(inputPath, 'tomte-0.4','config.yaml')]
    run_command(command)
    output = open(os.path.join("output","tomte-0.4","statistics.txt")).read()
    shutil.rmtree("output")
    return output

def runRalib(modelFile):
    command=[ 'java','-jar', os.path.join(libPath, 'ralib', "ralib_mod.jar"), "iosimulator", '-f', os.path.join(inputPath, 'ralib', "config.txt"), "target="+modelFile.replace("\\","/")]
    run_command(command, out=open("tempOut","w"))
    temp = open("tempOut")
    temp.seek(-1000, 2)
    output = temp.read()
    temp.close()
    #os.remove("tempOut")
    return output

def run_command(command,input=None,cwd=None, out=open('nul','w'), err=open('nul','w')):
    #print "running command: " + str(command)
    try:
      process = subprocess.Popen(command,cwd=cwd,stdout=out,stderr=err,stdin=subprocess.PIPE, bufsize = 65767)
      
    except Exception as inst:
      msg=""
      if cwd: 
         msg=msg+"\n\ncommand run in directory :\n    " + cwd
      print "problem with starting command : \n" + " ".join(command) + msg
      print str(inst)
    process.wait()

    if process.returncode:

#      if not stderrdata: stderrdata=""
#      msg="running command returned with error : \n   "  + join_command(command)
      msg="problem running command:\n\n      " + " ".join(command) + "\n"
      
      print msg
      print "return code ", process.returncode

    return process.returncode